productosRef.on('value', (snapshot) => {

    document.getElementById('contenedor').innerHTML = '';

    snapshot.forEach((productoSnapshot) => {
        const producto = productoSnapshot.val();


            const productoDiv = document.createElement('div');
            productoDiv.className = 'producto';


            const imagen = document.createElement('img');
            imagen.src = producto.url; 
            imagen.alt = producto.nombre; 
            imagen.className = 'producto-imagen';
            productoDiv.appendChild(imagen);

            const nombreParrafo = document.createElement('p');
            nombreParrafo.innerHTML = `<strong>Nombre:</strong> ${producto.nombre}`;
            productoDiv.appendChild(nombreParrafo);

            const precioParrafo = document.createElement('p');
            precioParrafo.innerHTML = `<strong>Precio:</strong> $${producto.precio}`;
            productoDiv.appendChild(precioParrafo);

            const codigoParrafo = document.createElement('p');
            codigoParrafo.innerHTML = `<strong>Código:</strong> ${producto.codigo}`;
            productoDiv.appendChild(codigoParrafo);

            const statusParrafo = document.createElement('p');
            statusParrafo.innerHTML = `<strong>Estado:</strong> ${producto.status}`;
            productoDiv.appendChild(statusParrafo);

            document.getElementById('contenedor').appendChild(productoDiv);
    });
});

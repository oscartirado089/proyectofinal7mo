const firebaseConfig = {
  apiKey: "AIzaSyBdeVeWnQJFkLrRSyYqj2JHbYxb4Nlgrrg",
  authDomain: "administrador-21593.firebaseapp.com",
  databaseURL: "https://administrador-21593-default-rtdb.firebaseio.com",
  projectId: "administrador-21593",
  storageBucket: "administrador-21593.appspot.com",
  messagingSenderId: "156388192080",
  appId: "1:156388192080:web:c12e631d00b929257e4655"
};

firebase.initializeApp(firebaseConfig);

const productosRef = firebase.database().ref('productos');
const storage = firebase.storage();
const storageRef = storage.ref();

function subirImagen() {
  const fileInput = document.getElementById('fileInput');
  const archivo = fileInput.files[0];
  const urlInput = document.getElementById('url');

  if (archivo) {

    if (archivo.type === 'image/jpeg' || archivo.type === 'image/png') {
      const imagenRef = storageRef.child(`imagenes/${archivo.name}`);


      imagenRef.put(archivo).then(() => {
        alert('Imagen subida con éxito.');


        imagenRef.getDownloadURL().then((url) => {
          const imagenContainer = document.getElementById('imagenContainer');
          const imagen = document.createElement('img');
          imagen.src = url;
          imagenContainer.appendChild(imagen);


          urlInput.value = url;
        }).catch((error) => {
          alert('Error al obtener la URL de descarga: ' + error.message);
        });
      }).catch((error) => {
        alert('Error al subir la imagen: ' + error.message);
      });
    } else {
      alert('Por favor, selecciona una imagen en formato .jpg, .jpeg o .png.');
    }
  } else {
    alert('Por favor, selecciona un archivo.');
  }
}


document.addEventListener("DOMContentLoaded", function () {
  const productForm = document.getElementById('productForm');


  productForm.addEventListener('submit', (e) => {
    e.preventDefault();

    const codigo = document.getElementById('codigo').value;
    const nombre = document.getElementById('nombre').value;
    const precio = document.getElementById('precio').value;
    const status = document.getElementById('status').value;
    const url = document.getElementById('url').value;


    if (codigo && nombre && precio && status && url) {

      productosRef.orderByChild('codigo').equalTo(codigo).once('value')
        .then((snapshot) => {
          if (snapshot.exists()) {
            const productoKeys = Object.keys(snapshot.val());
            const producto = snapshot.val()[productoKeys[0]];


            if (producto.status !== 'Deshabilitado') {
              alert('Ya existe un producto con este código y no está deshabilitado.');
            } else {

              const productoKey = productoKeys[0];
              productosRef.child(productoKey).update({
                nombre: nombre,
                precio: parseFloat(precio),
                status: status,
                url: url
              }).then(() => {
                alert('Producto actualizado correctamente.');

                productForm.reset();
              }).catch((error) => {
                alert('Error al actualizar el producto: ' + error.message);
              });
            }
          } else {

            const producto = {
              codigo: codigo,
              nombre: nombre,
              precio: parseFloat(precio),
              status: status,
              url: url,
            };

            productosRef.push(producto)
              .then(() => {
                alert('Producto agregado correctamente.');

                window.location.href = '/html/agregar.html';
              })
              .catch((error) => {
                alert('Error al agregar el producto: ' + error.message);
              });
          }
        })
        .catch((error) => {
          alert('Error al verificar el código del producto: ' + error.message);
        });
    } else {
      alert('Por favor, completa todos los campos.');
    }
  });
});


document.addEventListener("DOMContentLoaded", function () {

});


function buscarProducto() {
  const codigo = prompt('Ingrese el código del producto:');
  if (codigo) {
    productosRef.orderByChild('codigo').equalTo(codigo).once('value')
      .then((snapshot) => {
        if (snapshot.exists()) {
          const productoKey = Object.keys(snapshot.val())[0];
          const producto = snapshot.val()[productoKey];

          document.getElementById('codigo').value = producto.codigo;
          document.getElementById('nombre').value = producto.nombre;
          document.getElementById('precio').value = producto.precio;
          document.getElementById('status').value = producto.status;
          document.getElementById('url').value = producto.url;
        } else {
          alert('Producto no encontrado.');
        }
      })
      .catch((error) => {
        console.error('Error al buscar el producto:', error);
      });
  }
}


function deshabilitarProducto() {
  const codigo = prompt('Ingrese el código del producto que desea deshabilitar:');
  if (codigo) {
    productosRef.orderByChild('codigo').equalTo(codigo).once('value')
      .then((snapshot) => {
        if (snapshot.exists()) {
          snapshot.forEach((productoSnapshot) => {
            const productoKey = productoSnapshot.key;
            productosRef.child(productoKey).update({ status: 'Deshabilitado' })
              .then(() => {
                alert('Producto deshabilitado correctamente.');
              })
              .catch((error) => {
                alert('Error al deshabilitar el producto: ' + error.message);
              });
          });
        } else {
          alert('Producto no encontrado.');
        }
      })
      .catch((error) => {
        console.error('Error al buscar el producto:', error);
      });
  }
}



function actualizarProducto() {
  const codigo = prompt('Ingrese el código del producto que desea actualizar:');
  if (codigo) {
    const fileInput = document.createElement('input');
    fileInput.type = 'file';
    fileInput.accept = 'image/*'; 

    fileInput.onchange = (event) => {
      const file = event.target.files[0];
      if (file) {
        const storageRef = storage.ref(`imagenes/${file.name}`);
        storageRef.put(file).then(() => {

          storageRef.getDownloadURL().then((url) => {
            const nuevoNombre = prompt('Ingrese el nuevo nombre del producto:');
            const nuevoPrecio = prompt('Ingrese el nuevo precio del producto:');


            productosRef.orderByChild('codigo').equalTo(codigo).once('value')
              .then((snapshot) => {
                if (snapshot.exists()) {
                  snapshot.forEach((productoSnapshot) => {
                    const productoKey = productoSnapshot.key;
                    productosRef.child(productoKey).update({
                      nombre: nuevoNombre,
                      precio: parseFloat(nuevoPrecio),
                      url: url 
                    }).then(() => {
                      alert('Producto actualizado correctamente.');
                    }).catch((error) => {
                      alert('Error al actualizar el producto: ' + error.message);
                    });
                  });
                } else {
                  alert('Producto no encontrado.');
                }
              })
              .catch((error) => {
                console.error('Error al buscar el producto:', error);
              });
          }).catch((error) => {
            alert('Error al obtener la URL de descarga de la imagen: ' + error.message);
          });
        }).catch((error) => {
          alert('Error al subir la imagen: ' + error.message);
        });
      }
    };

    
    fileInput.click();
  }
}

function limpiarCampos() {
  document.getElementById('codigo').value = '';
  document.getElementById('nombre').value = '';
  document.getElementById('precio').value = '';
  document.getElementById('status').value = '';
  document.getElementById('url').value = '';

}

function logout() {

  window.location.href = '/html/login.html';
}